#ifndef DMA_H
#define DMA_H
#include <stm32f10x.h>
//interruption flag in ISR
#define GIF1  0x00000001
#define TCIF1 0x00000002
#define HTIF1 0x00000004
#define TEIF1 0x00000008
#define GIF2  0x00000010
#define TCIF2 0x00000020
#define HTIF2 0x00000040
#define TEIF2 0x00000080
#define GIF3  0x00000100
#define TCIF3 0x00000200
#define HTIF3 0x00000400
#define TEIF3 0x00000800
#define GIF4  0x00001000
#define TCIF4 0x00002000
#define HTIF4 0x00004000
#define TEIF4 0x00008000
#define GIF5  0x00010000
#define TCIF5 0x00020000
#define HTIF5 0x00040000
#define TEIF5 0x00080000
#define GIF6  0x00100000
#define TCIF6 0x00200000
#define HTIF6 0x00400000
#define TEIF6 0x00800000
#define GIF7  0x01000000
#define TCIF7 0x02000000
#define HTIF7 0x04000000
#define TEIF7 0x08000000

#define DMA1_Init() RCC->AHBENR |= 1

struct dma_config_info{
	DMA_Channel_TypeDef *channel;
	void *cpar;
	void *cmar;
	unsigned short cndtr;
	unsigned short ccr;
};

void DMA1_Config(struct dma_config_info *config_info);
#endif
