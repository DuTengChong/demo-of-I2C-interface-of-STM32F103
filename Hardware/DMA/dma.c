#include <dma.h>
#include <stm32f10x.h>
//#include <delay.h>

//可以在传输完成中断中做关闭接口、产生停止条件、禁止DMA请求等操作
/*void DMA1_Channel4_IRQHandler(void)
{
	//
	DMA1_Channel4->CCR &= 0xFFFFFFFE;
}*/

//为减少传递参数造成的损失，改成指针方式
void DMA1_Config(struct dma_config_info *config_info)
{
	DMA_Channel_TypeDef *DMA1_Channel = config_info->channel;
	//DMA_channel是结构体指针，算数运算时应该注意
	unsigned int mask = ((unsigned int)DMA1_Channel - DMA1_Channel1_BASE)/20;
	
	//传输过程中这些寄存器不可写
	DMA1_Channel->CCR &= 0xFFFFFFFE;
	mask <<= 2;
	DMA1->IFCR |= (0x01 << mask);
	
	DMA1_Channel->CPAR = (unsigned int)config_info->cpar;
	DMA1_Channel->CMAR = (unsigned int)config_info->cmar;
	DMA1_Channel->CNDTR = config_info->cndtr;
	//channel config
	DMA1_Channel->CCR = config_info->ccr;
	//ready to go
	DMA1->IFCR &= ~(0x0F << mask);
	DMA1_Channel->CCR |= 0x00000001;
}

/*void DAM1_Init(void)
{
	RCC->AHBENR |= 1;
	//delay_ms(2);
}*/

/*
void DMA1_channel4_start(void)
{
	DMA1_Channel4->CCR &= 0xFFFFFFFE;

	DMA1_Channel4->CCR |= 0x00000001;
}
*/
