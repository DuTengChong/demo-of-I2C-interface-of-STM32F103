#ifndef I2C_H
#define I2C_H
#include <stm32f10x.h>
#include <dma.h>
#include <sys.h>
//#include <led.h>
#include <GPIO_config.h>
//return 0:done
//return 1:can't send start conditon
//return 2:slave didn't ACK before finishing
//return 3:can't send ROM_addr
//return 4:input argument illegal
//return 5:AF is set
//return 6:can't get enough data
#define I2C_DEBUG 0
//config bit in CR1
#define PEC   0x1000
#define ACK   0x0400
#define STOP  0x0200
#define START 0x0100
#define ENGC  0x0040
#define ENPEC 0x0020
#define PE    0x0001
//config bit in CR2
#define LAST  0x1000
#define DMAEN 0x0800
#define ITBUF 0x0400
#define ITEVT 0x0200
#define ITERR 0x0100
//FLAG in SR1
#define AF 	  0x0400 	//ACK failure
#define ARLO  0x0200 	//Arbitrition lost
#define BERR  0x0100 	//Buss error: a wrong start/stop condition
#define TxE   0x0080 	//Data register empty(transmitter)
#define RxNE  0x0040 	//Data register not empty(receiver)
#define STOPF 0x0010 	//Stop detection(slave mode)
#define ADD10 0x0008 	//10-bit header sent(master mode)
#define BTF   0x0004 	//Byte transfer finish
#define ADDR  0x0002 	//Address sent(master mode)/matched(slave mode)
#define SB    0x0001 	//Start bit(master mode)
//Flag in SR2
#define DUALF 0x0080 	//Dual address flag(slave mode)
#define TRA   0x0004 	//Transmitter/receiver
#define BUSY  0x0002 	//Bus busy: SDA=0 or SCL=0
#define MSL   0x0001 	//Master/slave
//Flag in RT_flag
#define TxMODE  0x80 	//master transmitter mode
#define TCF		0x40 	//transmit complete flag
#define IN_ADDR 0x20 	//there is address in chip
#define STAGE   0x08 	//Stage 1 has completed.Address-in-chip transmitted
//extern unsigned int SR;
extern unsigned char ROM_addr;
extern unsigned char device_addr;
extern unsigned char RT_flag;
struct IIC_conf_info
{
	unsigned char slave_addr;
	unsigned char addr;		//address in chip
	unsigned char data_num;
	unsigned char flag;		//接收/发送状态标志
	void *data;				//buffer
};
void IIC1_Init(void);
void IIC2_Init(void);

unsigned char IIC1_DMA_Read(struct IIC_conf_info *config_info);
unsigned char E2PROM_Read(unsigned short addr,unsigned char *data,unsigned char data_num);
//unsigned char IIC2_ReadBytes(unsigned char addr,unsigned char *data,unsigned char data_num);
unsigned char IIC1_WriteBytes(unsigned char addr,unsigned char *data,unsigned char data_num);

#endif
